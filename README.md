# README

Media cocoapods repository for following libraries:

  - Infonline
  - InfonlineSurvey (IRSurvey)
  - IOMB new infonline library

#### Infonline:
##### 2.2.0
  - iphoneos binary was used to create zip
  - please zip files without root folder by using:
  ```
  zip -r X.zip .
  ```
