Pod::Spec.new do |spec|

	spec.name ='DAZNInfonlineSurvey'
	spec.version = '3.0.0'
	spec.homepage = 'https://git.infonline.de/IRSurveyLib'
	spec.source = { :http => 'https://bitbucket.org/GoalAdminFC11/ios-cocoapods/raw/0c5f175585f47e27c6136dc850566c1bc1ce91b2/Zip/DAZNInfonlineSurvey-3.0.0.zip' }
	spec.platform = :ios
	spec.vendored_frameworks = 'QdsInappWrapperLib.framework'
    spec.ios.frameworks = 'UIKit', 'AdSupport'

end
