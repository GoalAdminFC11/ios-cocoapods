Pod::Spec.new do |spec|

	spec.name ='DAZNInfonline'
	spec.version = '2.2.0'
	spec.homepage = 'https://git.infonline.de'
	spec.source = { :http => 'https://bitbucket.org/GoalAdminFC11/ios-cocoapods/raw/0c5f175585f47e27c6136dc850566c1bc1ce91b2/Zip/DAZNInfonline-2.2.0.zip' }
	spec.platform = :ios
	spec.vendored_frameworks = 'INFOnlineLibrary.framework'
    spec.ios.frameworks = 'UIKit', 'AdSupport'

end
