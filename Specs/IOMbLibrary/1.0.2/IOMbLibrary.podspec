Pod::Spec.new do |spec|

	spec.author = { 'INFOnline GmbH' => 'support@infonline.de' }
	spec.summary = "The IOMbLibrary is created for customers of INFOnline only, to track the use of their iOS apps."
	spec.name ='IOMbLibrary'
	spec.version = "1.0.2"
	spec.homepage = "https://repo.infonline.de/iom/base/sensors/app/ios"
	spec.source = { :http => 'https://bitbucket.org/GoalAdminFC11/ios-cocoapods/raw/7b2c77ae653781ec8ed2cc58f3d2887514459ecb/Zip/IOMbLibrary.zip' }
	spec.platform = :ios, "11.0"
	spec.vendored_frameworks = 'IOMbLibrary.xcframework'
  spec.ios.frameworks = 'UIKit', 'AdSupport'

end
